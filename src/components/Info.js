import React from "react";

class Info extends React.Component {
    render() {
        console.log(this.props);
        return (
            <div>
                My name is {this.props.firstname} {this.props.lastname} and my favourite number is {this.props.favNumber}
            </div>
        )
    }
}

export default Info;