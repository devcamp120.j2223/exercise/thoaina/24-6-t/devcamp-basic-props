import Info from "./components/Info";

function App() {
  return (
    <div>
      <Info firstname="Nguyễn" lastname="Thoại" favNumber={25}>Child info component</Info>
    </div>
  );
}

export default App;
